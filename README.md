NAME
====
vbfi - Visual Brainfuck Interpreter

SYNOPSIS
========
Usage: vbfi [option]

DEPENDENCIES
============
Nothing special except C standard libraries.

DESCRIPTION
===========
This is a brainfuck interpreter with the added option of a visual representation of what happens in memory.

The current implementation is not the most efficient, especially when using the Visual Mode. I might improve the speed of this interpreter, but this is currently not a priority. The main purpose of this interpreter is the educational value of being able to see how the code change memory.

Current design specifications of this interpreter:

    - Unlimited memory in both directions. This is implemented as a linked list.

    - 8-bit unsigned storage, so that wrapping can be utilized.

    - EOF will set the current cell to 0, or using the -e argument will leave memory unchanged.

    - Supports up to 1000 nested loops.


OPERATIONS
==========

-v  visual mode: Display visual representation of what happens in memory. This will produce a very large number of lines even for small programs. It is highly suggested to redirect output into a file when using this option.

-i PATH  
    input file: PATH must be a valid filesystem path to a file containing brainfuck code. The -i argument can be skipped, and instead just provide PATH directly.

-e  ignore eof: By default, this interpreter will set the current memory cell to 0 upon encountering EOF. This option will make the interpreter ignore EOF and leave memory unchanged.

AUTHOR
======
Daniel Jenssen <daerandin@gmail.com>

