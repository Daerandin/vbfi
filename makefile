# vbfi - Visual Brainfuck Interpreter

OUT = vbfi
CC = gcc
OBJ = vbfi.o
CFLAGS = -march=native -pthread -pedantic -Wall -Werror -Wextra -fstack-protector-strong -O2

vbfi: $(OBJ)
	$(CC) $(CFLAGS) -o $(OUT) $(OBJ)

vbfi.o: vbfi.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) $(OUT) $(OBJ)

.PHONY: clean
