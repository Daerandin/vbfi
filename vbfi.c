/*********************************************************************************************************
 *                                                                                                       *
 * vbfi - visual brainfuck interpreter                                                                   *
 *                                                                                                       *
 * Copyright (C) 2018 Daniel Jenssen <daerandin@gmail.com>.                                              *
 *                                                                                                       *
 * This program is free software: you can redistribute it and/or modify                                  *
 * it under the terms of the GNU General Public License as published by                                  *
 * the Free Software Foundation, either version 3 of the License, or                                     *
 * (at your option) any later version.                                                                   *
 *                                                                                                       *
 * This program is distributed in the hope that it will be useful,                                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                                        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                         *
 * GNU General Public License for more details.                                                          *
 *                                                                                                       *
 * You should have received a copy of the GNU General Public License                                     *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.                                 *
 *                                                                                                       *
 *                                                                                                       *
 *                                                                                                       *
 *                                                                                                       *
 * This is a brainfuck interpreter than can provide a visual representation of what is happening in      *
 * memory.                                                                                               *
 *                                                                                                       *
 * The program accepts the following arguments:                                                          *
 *                                                                                                       *
 *      -v  visual mode: Will display a visual representation of every step through the code.            *
 *          Currently, this will produce a very large number of lines even for small programs.           *
 *          Consider redirecting output into a file for better readability.                              *
 *                                                                                                       *
 *      -i PATH                                                                                          *
 *          input file: This argument must immediately be followed by the path file containing the       *
 *          brainfuck code. The actual -i argument can be skipped, and instead just directly provide     *
 *          the PATH.                                                                                    *
 *                                                                                                       *
 *      -e  ignore EOF: By default, the program sets EOF as 0 in memory. Passing this argument           *
 *          will make the interpreter ignore EOF on input and leave memory unchanged.                    *
 *                                                                                                       *
 * Example: vbfi -v -i /home/user/script                                                                 *
 *          vbfi /home/user/script                                                                       *
 *                                                                                                       *
 *********************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define MAX_LOOP 1001

#define OUTPUTSTRING 10000

/* Memory structure used for the program, a linked list. */
struct cell {
    struct cell *previous;
    struct cell *next;
    unsigned char value;
    long memlocation;
};

struct cell *create_cell(int);
struct cell *create_output();
struct cell *decrement(struct cell *, int *);
struct cell *increment(struct cell *, int *);
void free_list(struct cell *);
const char *visualize(struct cell*, const char);

int main(int argc, char **argv)
{
    bool print = true, visual_mode = false, eof_value = true;
    int c, input, mempos = 0, loopnumber = 0, loopcounter = 0;
    char *file_path = NULL;
    const char *vstring;
    FILE *codefile;
    struct cell *position, *outptr = NULL;
    long loopoffset[MAX_LOOP] = {0};

    /* First check arguments. */
    if (argc < 2) {
        printf("Error, missing path to script file.\n");
        exit(EXIT_FAILURE);
    }

    while (argc-- > 1) {
        /* If the argument begins with a '-' then we expect it to be either 'i' or 'v'. */
        if (**(++argv) == '-')
            switch (*(++(*argv))) {
                case 'i':
                    /* We assume that any argument following '-i' is the file path. */
                    file_path = *(++argv);
                    --argc;
                    break;
                case 'v':
                    visual_mode = true;
                    break;
                case 'e':
                    eof_value = false;
                    break;
                default:
                    printf("Error, invalid argument: -%s\n", *argv);
                    exit(EXIT_FAILURE);
            }
        /* Argument not starting with '-' is assumed to be file path. */
        else {
            file_path = *argv;
        }
    }
    /* We perform no particular error handling on file path, only error detection is if we can't open the file. */
    if (!(codefile = fopen(file_path, "r"))) {
        printf("Error, could not read the file: %s\n", file_path);
        exit(EXIT_FAILURE);
    }

    /* Create first cell, then start reading code from codefile. */
    position = create_cell(mempos);
    
    while ((c = getc(codefile)) != EOF) {
        if (visual_mode)
            print = true;

        switch (c) {
            case (int)',':
                input = getc(stdin);
                if (input == EOF) {
                    if (eof_value)
                        position->value = (unsigned char)0;
                } else
                    position->value = (unsigned char)input;
                break;
            case (int)'.':
                /* This is the most efficient and clean way to handle this. */
                if (!visual_mode)
                    putc((int)position->value, stdout);
                /* However, with visual mode we must handle it differently. */
                else {
                    /* Create first cell if it does not exist. */
                    if (!outptr)
                        outptr = create_output();
                    outptr->value = (position->value);
                    outptr->next = create_output();
                    (outptr->next)->previous = outptr;
                    outptr = outptr->next;
                }
                break;
            case (int)'+':
                (position->value)++;
                break;
            case (int)'-':
                (position->value)--;
                break;
            case (int)'<':
                position = decrement(position, &mempos);
                break;
            case (int)'>':
                position = increment(position, &mempos);
                break;
            case (int)'[':
                /* Start of loop if current memory value is not 0. */
                if (position->value) {
                    loopnumber++;
                    if (loopnumber >= MAX_LOOP) {
                        printf("\nError: Exceeded 1000 nested loops, which is not supported.\n");
                        exit(EXIT_FAILURE);
                    }
                    /* Store loop start position. */
                    loopoffset[loopnumber] = ftell(codefile) - 1;
                }
                else {
                    while (((c = getc(codefile)) != EOF && c != (int)']') || (c == (int)']' && loopcounter)) {
                        if (c == '[')
                            loopcounter++;
                        else if (c == ']' && loopcounter)
                            loopcounter--;
                    }
                    if (c == EOF) {
                        printf("Syntax error. Loop not closed.\n");
                        exit(EXIT_FAILURE);
                    }
                }
                break;
            case (int)']':
                fseek(codefile, loopoffset[loopnumber], SEEK_SET);
                loopnumber--;
                break;
            default:
                /* Skipping everything not part of regular syntax. */
                if (visual_mode)
                    print = false;
        }

        if (visual_mode && print) {
            vstring = visualize(position, (const char)c);
            printf("%s", vstring);
            while (outptr && outptr->previous)
                outptr = outptr->previous;
            if (outptr) {
                printf("\nProgram output:  ");
                while (outptr->next) {
                    putc((int)outptr->value, stdout);
                    outptr = outptr->next;
                }
            }
        }

    }
    fclose(codefile);

    if (loopnumber) {
        printf("Syntax error. Loop not closed.\n");
        exit(EXIT_FAILURE);
    }
    free_list(position);
    if (visual_mode)
        free_list(outptr);
    
    return 0;
}

/* Dynamically allocates memory for a new cell, initializes it and returns pointer to the cell. Use free() on pointer when done. */
struct cell *create_cell(int mempos)
{
    struct cell *ptr;

    if ((ptr = (struct cell *)malloc(sizeof(struct cell))) == NULL) {
        printf("Error, not enough memory to create more memory cells. Your system do not have enough memory to run the current program.\n");
        exit(EXIT_FAILURE);
    }
    ptr->previous = NULL;
    ptr->next = NULL;
    ptr->value = 0;
    ptr->memlocation = mempos;
    return ptr;
}

/* Wrapper to create output cells. */
struct cell *create_output(void)
{
    return create_cell(0);
}

/* Goes to previous location in memory, creating new cell if necessary. */
struct cell *decrement(struct cell *current, int *mempos)
{
    (*mempos)--;
    if (!(current->previous)) {
        current->previous = create_cell(*mempos);
        (current->previous)->next = current;
    }
    return current->previous;
}

/* Goes to next location in memory, creating new cell if necessary. */
struct cell *increment(struct cell *current, int *mempos)
{
    (*mempos)++;
    if (!(current->next)) {
        current->next = create_cell(*mempos);
        (current->next)->previous = current;
    }
    return current->next;
}

/* Free all dynamically allocated memory. */
void free_list(struct cell *position)
{
    struct cell *ptr = NULL;
    while (position->previous)
        position = position->previous;
    if (position->next) {
        ptr = position->next;
        ptr->previous = NULL;
    }
    free(position);
    if (ptr)
        free_list(ptr);
}

/* Create the visual representation of memory. */
const char *visualize(struct cell *position, const char instruction)
{
    static char tmpstr[10000];
    int i = 0;
    char nstr[50] = {'\0'};
    struct cell *start, *active, *stop;
    /* Pointers above are used to keep track of where to start and stop the current display. */

    strcpy(tmpstr, "\n\nCurrent command being run: ");
    strncat (tmpstr, &instruction, 1);
    strcat(tmpstr, "\n\n");
    active = position;
    while (position->previous && i++ < 6)
        position = position->previous;
    start = position;
    /* First write memory location numbers. */
    strcat(tmpstr, "Memory position   ");
    i = 0;
    while (position && i++ < 13) {
        strcat(tmpstr, "   ");
        snprintf(nstr, 50, "%3ld", position->memlocation);
        strcat(tmpstr, nstr);
        position = position->next;
    }
    stop = position;
    strcat(tmpstr, "\n");
    /* Next up is decimal value. */
    strcat(tmpstr, "Decimal value     ");
    position = start;
    while (position != stop) {
        strcat(tmpstr, "   ");
        snprintf(nstr, 50, "%03d", (int)position->value);
        strcat(tmpstr, nstr);
        position = position->next;
    }
    strcat(tmpstr, "\n");
    /* Next up is hexadecimal. */
    strcat(tmpstr, "Hexadecimal value ");
    position = start;
    while (position != stop) {
        strcat(tmpstr, "    ");
        snprintf(nstr, 50, "%02x", (int)position->value);
        strcat(tmpstr, nstr);
        position = position->next;
    }
    strcat(tmpstr, "\n");
    /* Lastly, show the current position. */
    strcat(tmpstr, "Current position  ");
    position = start;
    while (position != stop) {
        strcat(tmpstr, "     ");
        if (position == active)
            strcat(tmpstr, "^");
        else
            strcat(tmpstr, " ");
        position = position->next;
    }
    strcat(tmpstr, "\n");

    return tmpstr;
}
